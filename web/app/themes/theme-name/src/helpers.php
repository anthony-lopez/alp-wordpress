<?php

use Symfony\Component\VarDumper\Dumper\CliDumper;
use Symfony\Component\VarDumper\Dumper\HtmlDumper;

if ( ! function_exists( 'dd' ) ) {
    /**
     * Dump the passed variables and end the script.
     *
     * @param  mixed $args
     *
     * @return void
     */
    function dd( ...$args ) {
        http_response_code( 500 );
        foreach ( $args as $x ) {
            $dumper = in_array( PHP_SAPI, [ 'cli', 'phpdbg' ] ) ? new CliDumper : new HtmlDumper;

            ( new $dumper )->dump(
                ( new \Symfony\Component\VarDumper\Cloner\VarCloner() )->cloneVar( $x )
            );
        }
        die( 1 );
    }
}

if ( ! function_exists( 'asset' ) ) {
    /**
     * @param string $path
     *
     * @return string
     */
    function asset( $path ) {
        return get_template_directory_uri() . '/' . $path;
    }
}

if ( ! function_exists( 'alp_encode_emails' ) ) {
    function alp_encode_emails( $text ) {
        $email_reg = <<<REGEX
    @[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*\@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?@
REGEX;

        return preg_replace_callback( $email_reg, 'html_email_encode_callback', $text );
    }
}

if ( ! function_exists( 'html_email_encode_callback' ) ) {
    function html_email_encode_callback( $matches ) {
        return html_entity_encode_all( $matches[0] );
    }
}

if ( ! function_exists( 'html_entity_encode_all' ) ) {
    /**
     * Encode l'email des post_content et excerpt en entités HTML
     *
     * @param $text string texte à encoder
     *
     * @return string
     */
    function html_entity_encode_all( $text ) {
        $convmap = array( 0x000000, 0x10ffff, 0, 0xffffff );

        return mb_encode_numericentity( $text, $convmap, 'UTF-8' );
    }
}