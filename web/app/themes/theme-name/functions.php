<?php

add_action( 'after_setup_theme', function () {
    /**
     * Enable features from Soil when plugin is activated
     * @link https://roots.io/plugins/soil/
     */
    add_theme_support( 'soil-clean-up' );
    add_theme_support( 'soil-disable-asset-versioning' );
    add_theme_support( 'soil-disable-trackbacks' );
    add_theme_support( 'soil-js-to-footer' );
    add_theme_support( 'soil-nav-walker' );
    add_theme_support( 'soil-nice-search' );
    add_theme_support( 'soil-relative-urls' );

    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'html5', [ 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ] );
    add_theme_support( 'title-tag' );

    register_nav_menu( 'main', 'Menu principal' );
} );

add_action( 'wp_enqueue_scripts', function () {
    wp_deregister_script( 'jquery' );

    wp_enqueue_style( 'styles', asset( 'dist/app.css' ), [] );
    wp_enqueue_script( 'manifest', asset( 'dist/manifest.js' ), [] );
    wp_enqueue_script( 'vendor', asset( 'dist/vendor.js' ), [] );
    wp_enqueue_script( 'scripts', asset( 'dist/app.js' ), [] );
} );
