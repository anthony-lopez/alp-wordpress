<?php

/**
 * Replace all occurrences of the search string with the replacement string in the specified file
 *
 * @param mixed $search
 * @param mixed $replace
 * @param string $file File path
 */
function str_replace_file( $search, $replace, $file ) {
    $fileContent = file_get_contents( $file );
    $fileContent = str_replace( $search, $replace, $fileContent );
    file_put_contents( $file, $fileContent );
}

/**
 * @param string $string
 *
 * @return string
 */
function kebab_case( $string ) {
    return strtolower( preg_replace( '%([a-z])([A-Z])%', '\1-\2', $string ) );
}

/**
 * @param $string
 * @param bool $capitalizeFirstCharacter
 *
 * @return mixed|string
 */
function camel_case( $string, $capitalizeFirstCharacter = false ) {
    $str = str_replace( '-', '', ucwords( $string, '-' ) );
    if ( ! $capitalizeFirstCharacter ) {
        $str = lcfirst( $str );
    }

    return $str;
}

function convert_separator($subject)
{
    return str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, $subject);
}

$projectName       = kebab_case( basename( __DIR__ ) );
$themeDirectory    = convert_separator(__DIR__ . '/web/app/themes/theme-name');
$newThemeDirectory = convert_separator(__DIR__ . '/web/app/themes/' . $projectName);

// Rename theme directory with the project name
if ( file_exists( $themeDirectory ) ) {
    rename( $themeDirectory, $newThemeDirectory );
}
$themeDirectory = $newThemeDirectory;

// Update theme name in project files
str_replace_file( '{theme-name}', $projectName, convert_separator($themeDirectory . '/style.css') );
str_replace_file( '{theme-name}', $projectName, convert_separator($themeDirectory . '/webpack.mix.js') );
str_replace_file( '{theme-name}', $projectName, convert_separator(__DIR__ . '/composer.json') );
str_replace_file( '{ThemeName}', camel_case( $projectName, true ), convert_separator(__DIR__ . '/composer.json') );

// Self destruct
unlink( convert_separator(__DIR__ . '/setup.php') );